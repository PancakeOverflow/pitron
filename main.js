const { app, BrowserWindow, Menu, MenuItem, ipcRenderer: ipc } = require('electron');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win = null;

function replacer(e) {
  return e.replace('.', 'period')
    .replace('(', 'left_parenthesis')
    .replace(')', 'right_parenthesis')
    .replace('"', 'quote')
    .replace(/[\n\r]/, 'enter');
}

function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    width: 1200,
    height: 900,
    webPreferences: {
      nodeIntegration: true
    }
  });

  // build menu
  const menu = new Menu();
  const item = new MenuItem({
    label: 'test'
  });
  menu.append(item);
  // win.setMenu(menu);
  win.setMenu(null);
  win.webContents.openDevTools();

  // and load the index.html of the app.
  win.loadURL('https://www.youtube.com/watch?v=hHW1oY26kxQ').then(() => {
  });

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

app.on('window-all-closed', () => {
  app.quit()
});
